# Cast-Soft Cypress Automation

Here we are using [Cypress](https://www.cypress.io/) as an automation tool/

## Setup
1. clone it
2. `npm install`
3. `./node_modules/cypress/bin/cypress open`
4. Execute the `smokeTests` in Cypress app window
