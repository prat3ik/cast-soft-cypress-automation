context('Cast: Shop test cases', () => {
    beforeEach(() => {
        cy.visit("http://id.cast-soft.com/shop");
    });

    it('should navigate to shop page successfully', () => {
        // Verify that 4 products should be displayed
        cy.get('.product-info.py-4', {timeout: 4000}).should('be.visible');

        // Click on navigation button
        cy.get('#navbarDropdown').click();

        // Click on SIGN IN button from dropdown menu
        cy.get('.my-profile .dropdown-item[href*=\'login\']').click();
        cy.url().should('include', '/login/index/4');

        // Fill Email address, Password and submit
        cy.get('#email').type('nuhuhugiy@intempmail.com').should('have.value','nuhuhugiy@intempmail.com');
        cy.get("[type='submit']").click();
        cy.get('#password').type('Demo@123');
        cy.get("[type='submit']").click();

        // Verify user arrives on /shop page
        cy.url().should('include', '/login/login/logincheckfinal.html');
        cy.url().should('include', '/shop/');

        // TODO: Verify user should be logged in and it's username should be displayed

    });
});
